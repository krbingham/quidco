<?php
/**
 * @copyright (c) 2019.
 * @author Kyle Bingham kylebingham93@gmail.com
 *
 */

namespace Bingif\Classes;


/**
 * Class Gif
 * @package Bingif\Classes
 * Implements JsonSerializable allowing us to customize the Json Serializable function
 */
class Gif implements \JsonSerializable
{
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $name;

    /**
     * Gif constructor.
     * @param string $type
     * @param string $id
     * @param string $url
     * @param string $name
     */
    public function __construct(string $type, string $id, string $url, string $name)
    {
        $this->type = $type;
        $this->id = $id;
        $this->url = $url;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Finds any Gifs that matches the search Term
     * @param $searchTerm
     * @return array
     */
    static public function search($searchTerm)
    {
        $searchQueryString = [
            'api_key' => 'mlQYp1gL3mLR8GdcKMl1JuA5vHO4tQaY',
            'limit' => 5,
            'q' => $searchTerm
        ];

        $foundGifs = json_decode(file_get_contents('http://api.giphy.com/v1/gifs/search?' . http_build_query($searchQueryString)));

        $gifObjects = [];

        foreach ($foundGifs->data as $key => $gif) {
            $gifObjects[] = new self($gif->type, $gif->id, $gif->url, $gif->slug);
        }

        return $gifObjects;
    }


    /**
     * Returns an Random Gif
     * @return Gif
     */
    static public function random()
    {
        $searchQueryString = [
            'api_key' => 'mlQYp1gL3mLR8GdcKMl1JuA5vHO4tQaY'
        ];

        $foundGifs = json_decode(file_get_contents('http://api.giphy.com/v1/gifs/random?' . http_build_query($searchQueryString)));

        return new self($foundGifs->data->type, $foundGifs->data->id, $foundGifs->data->url, $foundGifs->data->slug);
    }

    /**
     * Returns the instance of $this as an JSON string
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
