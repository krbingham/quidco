<?php
/**
 * @copyright (c) 2019.
 * @author Kyle Bingham kylebingham93@gmail.com
 *
 */

namespace Bingif\Classes;

/**
 * Class Security
 * Global input clean up
 * @package Bingif\Classes
 */
class Security
{
    /**
     * Cleans the input from any characters such as < >
     * @param $input
     * @return mixed|null|string|string[]
     */
    public static function cleanup($input)
    {
        $input = str_replace('&', ' and ', $input);
        $input = strip_tags($input);
        $input = trim($input);
        $input = preg_replace('/[^a-zA-Z0-9\s\'\.\,\-\_\/\@\+\(\):]/', '', $input);
        $input = htmlentities($input, ENT_QUOTES);
        $input = str_replace('&#039;', '\'', $input);
        return $input;
    }

    /**
     * Checks the headers for the Api Key and makes sure the key matches
     * @return bool
     */
    public static function checkApiKey()
    {
        $headers = getallheaders();
        if ($headers['Api_Key'] === "*FhW57T7unksAXED7uDAAt3q*1M4*")
        {
            return true;
        }

        return false;
    }
}