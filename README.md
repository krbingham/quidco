#Quidco Task
This is built using OOP in mind as well as PHPUnit and PHPDoc comment blocks.

To run the program you will need to have PHP and composer installed.

This as been tested on PHP 7.2 and may not work on older versions.

##Setup

Install composer package PHPUnit
````bash
composer install
````

You can test the API by running the PHPUnit tests:

````bash
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/GifTest
````

To run PHP as a server run the following command.

````php
php -S localhost:8000
````

##How to use
There are two routes search and random.

You will need to pass the following in the HTTP header to be authorised to access the routes:

Api_Key - *FhW57T7unksAXED7uDAAt3q*1M4*

To retrive a random gif:

````URL
http://localhost:8000/random.php
````

To search for a gif with a key word

````URL
localhost:8000/search.php?q=hello world
````
