<?php
/**
 * @copyright (c) 2019.
 * @author Kyle Bingham kylebingham93@gmail.com
 *
 */

namespace Bingif;
use Bingif\Classes\Security;
use Bingif\Classes\Gif;

header('Content-Type: application/json');


/**
 * Requiring the classes to allow php to find the name spaces
 */
require('Classes/Security.php');
require('Classes/Gif.php');

/**
 * Checking if the request is legitimate by the header Api Key
 */
if(Security::checkApiKey()) {
    /**
     * Sending the cleaned searchTerm to the static method in search
     * Allowing the method to be called anywhere without instantiating the class
     * This will return an array containing instances of Gif
     * We are then using the jsonSerialize to encode it to JSON
     * This gives the developer the option to do other operations with the
     * Gif Object if required
     */
    echo(json_encode(Gif::random()));
} else {
    /**
     * If the request doesn't have or doesn't match the Api Key it will return with the
     * HTTP response code 401 and a message
     */
    echo(json_encode([
        'status' => 401,
        'message' => 'Not Authorized - Provide Header Api Key'
    ]));
}