<?php
/**
 * @copyright (c) 2019.
 * @author Kyle Bingham kylebingham93@gmail.com
 *
 */

require('Classes\Gif.php');

use Bingif\Classes\Gif;
use PHPUnit\Framework\TestCase;

/**
 * Class GifTest
 */
class GifTest extends TestCase
{
    /**
     * Testing the Random Gif:class static function
     * Making sure it returns an instance of Gif
     */
    public function testCountRandom()
    {
        $this->assertInstanceOf(
            Gif::class,
            Gif::random()
        );
    }

    /**
     * Testing the Search Gif:class static function
     * Making sure it returns 5 results
     * As well making sure each element is an instance of Gif
     */
    public function testSearch()
    {
        $testGifs = Gif::search('Hello World');

        $this->assertCount(
            5,
            $testGifs
        );

        foreach ($testGifs as $gif) {
            $this->assertInstanceOf(
                Gif::class,
                $gif
            );
        }
    }
}
